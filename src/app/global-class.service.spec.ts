import { TestBed } from '@angular/core/testing';

import { GlobalClassService } from './global-class.service';

describe('GlobalClassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalClassService = TestBed.get(GlobalClassService);
    expect(service).toBeTruthy();
  });
});
