import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Category } from '../Model/Category';
import { Categories } from '../mock-data/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor() { }

  getCategories():Observable<Category[]>{
    return of(Categories);
  }
}
