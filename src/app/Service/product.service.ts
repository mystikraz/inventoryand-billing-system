import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from '../Model/Product';
import { product } from '../mock-data/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  getProducts():Observable<Product[]>{
    return of(product);
  }
}
