import { Component, OnInit } from '@angular/core';
import { GlobalClassService } from '../global-class.service';
import { ProductService } from '../Service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products;

  constructor(
    private productService: ProductService,
    private globalClass: GlobalClassService
  ) { }

  ngOnInit() {
    this.getProducts();
  }
  getProducts() {
    this.productService.getProducts()
    .subscribe(x=>this.products=x);
    this.globalClass.addProduct(this.products);
  }
}
