import { Component, OnInit } from '@angular/core';
import { GlobalClassService } from 'src/app/global-class.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-createproduct',
  templateUrl: './createproduct.component.html',
  styleUrls: ['./createproduct.component.css']
})
export class CreateproductComponent implements OnInit {
productId;
product;
createProductForm;
  constructor(
    private globalClass:GlobalClassService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    ) { }

  ngOnInit() {
    this.productId = this.route.snapshot.params['id'];

    this.createProductForm = this.formBuilder.group({
      title: '',
      description: '',
    });
    this.product=this.globalClass.product.find(x=>x.id==this.productId)
    this.populateForm();
  }
  populateForm() {
    this.createProductForm.patchValue({
      // id: this.user.id,
      name: this.product.name,
      category: this.product.category,
      description: this.product.description,
      rate: this.product.rate      
    });
  }

}
