import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UsersComponent } from './users/users.component';
import { CreateuserComponent } from './users/createuser/createuser.component';
import { CategoryComponent } from './category/category.component';
import { CreateCategoryComponent } from './category/create-category/create-category.component';
import { ProductComponent } from './product/product.component';
import { CreateproductComponent } from './product/createproduct/createproduct.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    CreateuserComponent,
    CategoryComponent,
    CreateCategoryComponent,
    ProductComponent,
    CreateproductComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
