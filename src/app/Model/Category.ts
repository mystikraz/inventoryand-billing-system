export class Category{
    id:number;
    title:string;
    description:string;
    added_date:Date;
}