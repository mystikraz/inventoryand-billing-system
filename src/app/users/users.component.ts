import { Component, OnInit } from '@angular/core';
import { UserService } from '../Service/user.service';
import { User } from '../Model/User';
import { GlobalClassService } from '../global-class.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  constructor(private userService: UserService,private globalClass:GlobalClassService) { }

  ngOnInit() {
    this.getUsers();
  }
  getUsers() {
    this.userService.getAllUsers()
    .subscribe(x=>this.users=x);
    // this.globalClass.user=this.users;
    this.globalClass.addUser(this.users);
    console.log(this.users);
}
}
