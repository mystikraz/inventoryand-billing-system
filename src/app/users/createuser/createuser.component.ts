import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Users } from 'src/app/mock-data/UserDetails';
import { FormBuilder } from '@angular/forms';
import { GlobalClassService } from 'src/app/global-class.service';

@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.css']
})
export class CreateuserComponent implements OnInit {
  userId;
  user;
  createUserForm;
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private globalClass: GlobalClassService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.user = Users[+params.get('user.id')];
    })
    this.userId = this.route.snapshot.params['id'];

    this.createUserForm = this.formBuilder.group({
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      status: '',
      color: '',
      country: '',
      role: '',
      gender: '',
      address: '',
    });
    this.user=this.globalClass.user.find(x=>x.id==this.userId)
    // this.user = this.globalClass.user.filter(x => x.id == this.userId);
    this.populateForm();
  }

  onSubmit(userData) {
    this.globalClass.user.push(userData);
    console.warn('User has been saved', userData);
    this.createUserForm.reset();
  }
  populateForm() {
    // if (!this.user) return;
    this.createUserForm.patchValue({
      // id: this.user.id,
      email: this.user.email,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      password: this.user.password,
      status: this.user.status,
      color: this.user.color,
      country: this.user.country,
      role: this.user.userType,
      gender: this.user.gender,
      address: this.user.address,
    });
  }


}
