import { Category } from '../Model/Category';

export const Categories: Category[] = [
    { id: 1, title: 'Fruit', description: 'Mango items', added_date: new Date() },
    { id: 2, title: 'Food', description: 'Mango items', added_date: new Date() },
];