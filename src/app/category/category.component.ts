import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../Service/category.service';
import { Category } from '../Model/Category';
import { GlobalClassService } from '../global-class.service';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: Category[];
  constructor(private categoryService: CategoryService,private globalClass:GlobalClassService) { }

  ngOnInit() {
    this.getCategories();
  }addCategory
  getCategories() {
    this.categoryService.getCategories()
      .subscribe(x => this.categories = x);
  this.globalClass.addCategory(this.categories);
  }

 
}
