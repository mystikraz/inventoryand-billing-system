import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Category } from 'src/app/Model/Category';
import { Categories } from 'src/app/mock-data/Category';
import { FormBuilder } from '@angular/forms';
import { GlobalClassService } from 'src/app/global-class.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {
  categoryId;
  category;
  createCategoryForm;
  constructor(
    private route:ActivatedRoute,
    private formBuilder: FormBuilder,
    private globalClass: GlobalClassService
    ) { }

  ngOnInit() {
    
    this.categoryId = this.route.snapshot.params['id'];

    this.createCategoryForm = this.formBuilder.group({
      title: '',
      description: '',
    });
    this.category=this.globalClass.category.find(x=>x.id==this.categoryId)
    this.populateForm();
  }
  
  populateForm(){
    this.createCategoryForm.patchValue({
      title:this.category.title,
      description:this.category.description
    })
  }

}
