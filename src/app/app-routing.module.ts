import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { CreateuserComponent } from './users/createuser/createuser.component';
import { CategoryComponent } from './category/category.component';
import { CreateCategoryComponent } from './category/create-category/create-category.component';
import { ProductComponent } from './product/product.component';
import { CreateproductComponent } from './product/createproduct/createproduct.component';

const routes:Routes=[
{path:'Users',component:UsersComponent},
{path:'newuser/:id',component:CreateuserComponent},
{path:'newuser',component:CreateuserComponent},
{path:'Category',component:CategoryComponent},
{path:'create-category',component:CreateCategoryComponent},
{path:'create-category/:id',component:CreateCategoryComponent},
{path:'Products',component:ProductComponent},
{path:'createproduct',component:CreateproductComponent},
{path:'createproduct/:id',component:CreateproductComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
