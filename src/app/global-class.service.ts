import { Injectable } from '@angular/core';
import { User } from './Model/User';
import { Category } from './Model/Category';
import { Product } from './Model/Product';

@Injectable({
  providedIn: 'root'
})
export class GlobalClassService {
  user: User[];
  category: Category[];
  product: Product[];
  constructor() { }

  addUser(user: User[]) {
    this.user = user;
  }
  addCategory(category: Category[]) {
    this.category = category;
  }
  addProduct(product: Product[]) {
    this.product = product;
  }
}
